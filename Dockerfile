FROM php:5.6.15-fpm

RUN apt-get update \
 && curl -sL https://deb.nodesource.com/setup | bash - \
 && apt-get install -y git libssl-dev zlib1g-dev libicu-dev g++ \
 && docker-php-ext-install zip mbstring intl mysql mysqli pdo pdo_mysql

ADD docker/php.ini /usr/local/etc/php/php.ini

WORKDIR /var/www/orientdb